describe("MathUtils", () => {

    describe("::isEven() - test parité", () => {
        it("0 est pair", () => {
            expect(MathUtils.isEven(0)).toBeTrue();
        });
    
        it("1 est impair", () => {
            expect(MathUtils.isEven(1)).toBeFalse();
        });
    
        it("2 est pair", () => {
            expect(MathUtils.isEven(2)).toBeTrue();
        });
    
        it("3 est impair", () => {
            expect(MathUtils.isEven(3)).toBeFalse();
        });
    
        it("999998 est pair", () => {
            expect(MathUtils.isEven(999998)).toBeTrue();
        });
    
        it("999999 est ipair", () => {
            expect(MathUtils.isEven(999999)).toBeFalse();
        });
    })


});